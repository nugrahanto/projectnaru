<?php include ('./header.php'); ?>
<body>
	<!--==========================
		about Section
	============================-->
	<section id="about">
		<div class="container">
			<div class="row">
				<div class="col-md-12 ab-parent">
					<div class="ab-child ab-header">
						about us
					</div>
					<div class="ab-child ab-logo">
						<a href="./" id="fade"><img src="./assets/logo.png"></a>
					</div>
				</div>

				<div class="col-md-12 about-content">
					<span class="f-upp"><b>project naru</b></span>
					<span>is a multi discipliary studio containt of broad services related with human living ergonomically and sustainably.</span>
					<br>
					<br>
					<span>Starting from</span>
					<span class="f-upp"><a href="http://www.behance.net/advancemediastudio" id="fade">ADVANCE MEDIA STUDIO</a></span>
					<span> in 2012 as Interior Architect, Kekka as the compliments in Product Design and Toedjoeh as the newest project coming in 2019. The company is developing a passion for design of various eras, cultures, origins and styles.</span>
					<br>
					<br>
					<span>PROJECT NARU</span>
					<span>approach to design involves flexibility in working with existing, knowing what can and should be sustain and which to eliminate to allow the new growth. Team up with various young emerging designer all over Indonesia, we strive for sensible solutions that incorporate sustainable design strategies. We aim for functional, engaging, impressive spaces that foster productivity. Attention to detail and extensive documentation paired with our fluid design approach set us apart from the rest.
					</span>
					<br>
					<br>
					<br>
					<span class="f-upp"><b>kekka</b></span>
					<span>"Kekka is defined as result, and outcome, coming in fruition".</span>
					<br>
					<br>
					<span>Our philosophy is to provide result in product designs such as lighting, accessories, home accessories and furniture. We believe that good design can only be fruitful through fundamental understanding of people.</span>
					<br>
					<br>
					<span>We certainly re-invented through collections and series of different design, collaborating with talented craftmen around Indonesia.</span>
					<br>
					<br>
					<span>Our company vision is to promote the views that human hold so dear and encourage all of us to live more meaningful lives, not to simply throw everything away.</span>
					<br>
					<br>
					<br>
					<span class="f-upp"><b>toedjoeh</b></span>
					<span>Toedjoeh started in 2019, focusing on set designs and décor such as fashion photo set, exhibitions, fashion show set, party decoration which includes graphic, visualization and merchandisings.</span>
					<br>
					<br>
					<span>Our approach is driven by endless curiosity, design capabilities and style to surpasses trend. It is very important for Toedjoeh to deliver and manifest the dreams of the clients. “Sampai ke Toedjoehan”.</span>
				</div>
			</div>
		</div>
	</section>

	<!-- Footer section -->
	<footer></footer>

	<!-- Button Up -->
	<a href="#head" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

	<!-- JavaScript Libraries -->
	<script src="lib/jquery/jquery.min.js"></script>
	<script src="lib/jquery/jquery-migrate.min.js"></script>
	<script src="lib/bootstrap/js/bootstrap.bundle.min.js"></script>

	<script>
		//Page fade in when load
		$(window).load(function() {
			$("body").animate({opacity: 1}, 1000);
		});

		$(document).ready(function() {
			//Page fade out when unload
			$( "#fade" ).click(function(e) {
				e.preventDefault();
				$link = $(this).attr("href");
				$("body").animate({opacity: 0}, 1000, function(){
					window.location = $link;
				});
			});

			// Add smooth scrolling to all links
			$("a").on('click', function(event) {

				// Make sure this.hash has a value before overriding default behavior
				if (this.hash !== "") {

					// Prevent default anchor click behavior
					event.preventDefault();

					// Store hash
					var hash = this.hash;

					// Using jQuery's animate() method to add smooth page scroll
					// The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
					$('html, body').animate({
						scrollTop: $(hash).offset().top
					}, 800, function(){

						// Add hash (#) to URL when done scrolling (default click behavior)
						window.location.hash = hash;
					});
				} // End if
			});
		});

		/*$(document).ready(function(){
			
			//Page fade in when load
			$('body').hide().fadeIn(2000);

			//Page fade out when unload
			$( "#fade" ).click(function(e) {
				e.preventDefault();
				$link = $(this).attr("href");
				$("body").fadeOut(500,function(){
					window.location =  $link;
				});
			});

			$("a").click(function(e) {
				e.preventDefault();
				$link = $(this).attr("href");
				$("body").fadeOut(200,function(){
					window.location =  $link;
				});
			});
			
			
		});*/
	</script>

</body>
</html>