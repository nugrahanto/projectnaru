<?php 
	function url(){
		return sprintf(
			"%s://%s%s",
			isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http',
			$_SERVER['SERVER_NAME'], "/projectnaru/"
		);
	}
?>

<!DOCTYPE html>
<html lang="en">
<html>
<head>
	<meta charset="utf-8">
	<title>Project Naru</title>
	<meta content="width=device-width, initial-scale=1.0" name="viewport">
	<meta content="" name="keywords">
	<meta content="" name="description">

	<!-- Base Url -->
	<base href="<?php echo url() ?>">
	
	<!-- Bootstrap CSS File -->
	<link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

	<!-- Font Awesome CSS Files -->
	<link href="lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">

	<!-- Carousel Stylesheet File -->
	<link href="css/carousel.css" rel="stylesheet">
	
	<!-- Main Stylesheet File -->
	<link href="css/style.css" rel="stylesheet">

	<!-- JS File -->
</head>