<?php include ('./header.php'); ?>
<body>

	<!--==========================
		head Section
	============================-->
	<section id="head">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-11 col-md-10 col-lg-11 col-xl-11">
					<a href="./about.php" id="fade"><span class="f-upp">project naru</a></span> is a 
					multi-disciplinary studio contain of broad services related with human living ergonomically and sustainable <a href="./projects/" id="fade">projects</a>.
					<a href="#contact"><span class="f-capz">contact</span></a> us for further
					information. Cheers</font>
			</div>
		</div>
	</section>

	<!--==========================
		project Section
	============================-->
	<section id="project">
		<div class="container">
			<div class="row">
				<div class="col-md-12 ab-parent">
					<div class="ab-child">
						<a href="./projects/public-space-vosco-coffee-sby.php" style="color: black;">
							<div class="home-project">
							<div class="home-project-img"><img src="./assets/projects/ps-vosco-coffee-sby/cover.jpg"></div>
							<div class="home-project-title">PUBLIC SPACE</div>
							</div>
						</a>
					</div>
					<div class="ab-child">
						<div class="logo"><img src="./assets/logo.png"></div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<!--==========================
		contact Section
	============================-->
	<section id="contact">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-md-9 col-lg-10 col-xl-10">
					you can contact me via <a href="mailto: hi@projectnaru.com">mail</a> or <a href="http://www.instagram.com/projectnaru" id="fade">instagram</a>
				</div>
			</div>
		</div>
	</section>

	<!-- Footer section -->
	<footer></footer>

	<!-- Button Up -->
	<a href="#head" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

	<!-- JavaScript Libraries -->
	<script src="lib/jquery/jquery.min.js"></script>
	<script src="lib/jquery/jquery-migrate.min.js"></script>
	<script src="lib/bootstrap/js/bootstrap.bundle.min.js"></script>

	<script>
		//Page fade in when load
		$(window).load(function() {
			//$("body").animate({opacity: 1}, 1000);
		});

		$(document).ready(function() {
			$("body").animate({opacity: 1}, 1000);
			//Page fade out when unload
			$( "#fade" ).click(function(e) {
				e.preventDefault();
				$link = $(this).attr("href");
				$("body").animate({opacity: 0}, 1000, function(){
					window.location = $link;
				});
			});

			// Add smooth scrolling to all links
			$("a").on('click', function(event) {

				// Make sure this.hash has a value before overriding default behavior
				if (this.hash !== "") {
					// Prevent default anchor click behavior
					event.preventDefault();

					// Store hash
					var hash = this.hash;

					// Using jQuery's animate() method to add smooth page scroll
					// The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
					$('html, body').animate({
						scrollTop: $(hash).offset().top
					}, 800, function(){

						// Add hash (#) to URL when done scrolling (default click behavior)
						window.location.hash = hash;
					});
				} // End if
			});
		});
	</script>

</body>
</html>