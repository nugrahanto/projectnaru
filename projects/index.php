<?php 
	include ('../header.php'); 

	$content = array (
		array(
			"name"=>"BOOTH - SANG PISANG MATOS", 
			"link"=>"booth-sang-pisang-matos", 
			"dir"=>"booth-sang-pisang-matos"),
		array(
			"name"=>"BOOTH - SANG PISANG MOG", 
			"link"=>"booth-sang-pisang-mog", 
			"dir"=>"booth-sang-pisang-mog"),
		array(
			"name"=>"BOOTH - SANG PISANG TRANSMART", 
			"link"=>"booth-sang-pisang-transmart", 
			"dir"=>"booth-sang-pisang-transmart"),
		array(
			"name"=>"PUBLIC SPACE – PROTIGA MALANG", 
			"link"=>"public-space-protiga-malang", 
			"dir"=>"ps-protiga-malang"),
		array(
			"name"=>"PUBLIC SPACE – MIE SETAN PASURUAN", 
			"link"=>"public-space-mie-setan-pasuruan", 
			"dir"=>"ps-mie-setan-pasuruan"),
		array(
			"name"=>"PUBLIC SPACE – JATIM PARK 3 – LEFT WING DINO CAFE AREA", 
			"link"=>"public-space-jatim-park-3-left-wing-dino-cafe-area", 
			"dir"=>"ps-jp3-dino-cafe-area"),
		array(
			"name"=>"PUBLIC SPACE – LOTENG MALANG", 
			"link"=>"public-space-loteng-malang", 
			"dir"=>"ps-loteng-malang"),
		array("name"=>
			"PUBLIC SPACE – MIE SETAN LAWANG", 
			"link"=>"public-space-mie-setan-lawang", 
			"dir"=>"ps-mie-setan-lawang"),
		array(
			"name"=>"PUBLIC SPACE – JATIM PARK 1 – KENDEDES AREA", 
			"link"=>"public-space-jatim-park-1-kendedes-area", 
			"dir"=>"ps-jp1-kendedes-area"),
		array(
			"name"=>"PUBLIC SPACE – MOMMY MANGO TRANSMART", 
			"link"=>"public-space-mommy-mango-transmart", 
			"dir"=>"ps-mommy-mango-transmart"),
		array(
			"name"=>"PUBLIC SPACE – NOMU 9 MALANG", 
			"link"=>"public-space-nomu-9-malang", 
			"dir"=>"ps-nomu-9-malang"),
		array(
			"name"=>"PUBLIC SPACE – PAWVILION DOG CAFE MALANG", 
			"link"=>"public-space-pawvilion-dog-cafe-malang", 
			"dir"=>"ps-pawvilion-dog-cafe-malang"),
		array(
			"name"=>"PUBLIC SPACE – S PROJECT MALANG CONCEPT SEMERU", 
			"link"=>"public-space-s-project-malang-concept-semeru", 
			"dir"=>"ps-s-project-malang-concept-semeru"),
		array(
			"name"=>"PUBLIC SPACE – VOSCO COFFEE SBY", 
			"link"=>"public-space-vosco-coffee-sby", 
			"dir"=>"ps-vosco-coffee-sby"),
		array(
			"name"=>"RESIDENTAL – BANGSRI MALANG", 
			"link"=>"residential-bangsri-malang", 
			"dir"=>"residential-bangsri-malang"),
		array(
			"name"=>"RESIDENTAL – GB 1 HOUSE MALANG", 
			"link"=>"residential-gb-1-house-malang", 
			"dir"=>"residential-gb-1-house-malang"),
		array(
			"name"=>"RESIDENTAL – GB 2 HOUSE MALANG", 
			"link"=>"residential-gb-2-house-malang", 
			"dir"=>"residential-gb-2-house-malang"),
		array(
			"name"=>"RESIDENTAL – A HOUSE MALANG CONCEPT", 
			"link"=>"residential-a-house-malang-concept", 
			"dir"=>"residential-a-house-malang-concept"),
		array(
			"name"=>"RESIDENTAL – M HOUSE MALANG CONCEPT", 
			"link"=>"residential-m-house-malang-concept", 
			"dir"=>"residential-m-house-malang-concept"),
		array(
			"name"=>"RESIDENTAL – N HOUSE MALANG", 
			"link"=>"residential-n-house-malang", 
			"dir"=>"residential-n-house-malang"),
		array(
			"name"=>"RESIDENTAL – R HOUSE 2ND FLOOR ADDITION", 
			"link"=>"residential-r-house-2nd-floor-addition", 
			"dir"=>"residential-r-house-2nd-floor-addition"),
		array(
			"name"=>"RESIDENTAL – S HOUSE MALANG", 
			"link"=>"residential-s-house-malang", 
			"dir"=>"residential-s-house-malang")
	);
?>
<body>
	<!--==========================
		Head Section
	============================-->
	<section id="head">
		<div class="container">
			<div class="row">
				<div class="col-md-12 ab-parent" style="z-index: 1000;">
					<div class="ab-child ab-header">
						portofolio
					</div>
					<div class="ab-child ab-logo">
						<a href="./" id="fade"><img src="./assets/logo.png"></a>
					</div>
				</div>

				<div class="col-md-12 portofolio-link-wrapper">
					<div class="portofolio-link"><a href="./projects/">advance media studio</a></div>
					<div class="portofolio-link"><a href="./projects/toedjoeh.php">toedjoeh</a></div>
					<div class="portofolio-link"><a href="./projects/">kekka</a></div>
				</div>

				<div class="col-md-12 portofolio-wrapper">
					<div class="row">
						<?php for ($i=0; $i < count($content); $i++) : ?>
						<div class="col-md-6 col-sm-6">
							<a href="./projects/<?php echo $content[$i]['link']; ?>.php" style="color: black;"><div class="portofolio-list">
								<div class="portofolio-head"><?php echo $content[$i]['name']; ?></div>
								<div class="portofolio-img"><img src="./assets/projects/<?php echo $content[$i]['dir']; ?>/cover.jpg"></div>
							</div></a>
						</div>
						<?php endfor; ?>
					</div>
				</div>
			</div>
		</div>
	</section>

	<!-- Footer section -->
	<footer></footer>

	<!-- Button Up -->
	<a href="#head" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

	<!-- JavaScript Libraries -->
	<script src="lib/jquery/jquery.min.js"></script>
	<script src="lib/jquery/jquery-migrate.min.js"></script>
	<script src="lib/bootstrap/js/bootstrap.bundle.min.js"></script>

	<script>
		//Page fade in when load
		$(window).load(function() {
			$("body").animate({opacity: 1}, 1000);
		});

		$(document).ready(function() {
			//Page fade out when unload
			$( "#fade" ).click(function(e) {
				e.preventDefault();
				$link = $(this).attr("href");
				$("body").animate({opacity: 0}, 1000, function(){
					window.location = $link;
				});
			});

			// Add smooth scrolling to all links
			$("a").on('click', function(event) {

				// Make sure this.hash has a value before overriding default behavior
				if (this.hash !== "") {
					// Prevent default anchor click behavior
					event.preventDefault();

					// Store hash
					var hash = this.hash;

					// Using jQuery's animate() method to add smooth page scroll
					// The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
					$('html, body').animate({
						scrollTop: $(hash).offset().top
					}, 800, function(){

						// Add hash (#) to URL when done scrolling (default click behavior)
						window.location.hash = hash;
					});
				} // End if
			});
		});

		/*$(document).ready(function(){
			
			//Page fade in when load
			$('body').hide().fadeIn(2000);

			//Page fade out when unload
			$( "#fade" ).click(function(e) {
				e.preventDefault();
				$link = $(this).attr("href");
				$("body").fadeOut(500,function(){
					window.location =  $link;
				});
			});

			$("a").click(function(e) {
				e.preventDefault();
				$link = $(this).attr("href");
				$("body").fadeOut(200,function(){
					window.location =  $link;
				});
			});
			
			
		});*/
	</script>

</body>
</html>