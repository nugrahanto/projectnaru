<?php 
	include ('../header.php');

	$tittle  = "RESIDENTAL – GB 2 HOUSE MALANG";
	$dir 	 = "residential-gb-2-house-malang";
	$content = array (
		array(
			"img" => "GB2-1"),
		array(
			"img" => "GB2-2"),
		array(
			"img" => "GB2-3"),
		array(
			"img" => "GB2-4"),
		array(
			"img" => "GB2-5"),
		array(
			"img" => "GB2-6"),
		array(
			"img" => "GB2-7"),
		array(
			"img" => "GB2-8")
	);
?>
<body>
	<!--==========================
		Head Section
	============================-->
	<section id="head">
		<div class="container">
			<div class="row">
				<div class="col-md-12 ab-parent" style="z-index: 1000;">
					<div class="ab-child ab-header">
						portofolio
					</div>
					<div class="ab-child ab-logo">
						<a href="./" id="fade"><img src="./assets/logo.png"></a>
					</div>
				</div>

				<div class="col-md-12 portofolio-link-wrapper">
					<a href="./projects/"><div class="portofolio-link">
						<?php echo $tittle; ?>
					</div></a>
				</div>

				<div class="col-md-12">
					<div class="row">
						<?php for ($i=0; $i < count($content); $i++) : ?>
						<div class="col-md-8">
							<div class="content-wrapper">
								<img id="myImg" src="./assets/projects/<?php echo $dir; ?>/<?php echo $content[$i]["img"]; ?>.jpg" onClick="document.getElementById('myModal').style.display='block';document.getElementById('ind-<?php echo $i; ?>').classList.add('active');document.getElementById('itm-<?php echo $i; ?>').classList.add('active');">
							</div>
						</div>
						<?php endfor; ?>
					</div>
				</div>
			</div>
		</div>

		<div class="container">
			
		</div>
	</section>

	<!--==========================
		Head Section
	============================-->

	<!-- The Modal -->
	<div id="myModal" class="modal">

		<!-- The Close Button -->
		<span class="close">&times;</span>
		<div class="modal-content">

			<div id="slides" class="carousel slide" data-ride="carousel">
				<ol class="carousel-indicators">
					<?php for ($i=0; $i < count($content); $i++) : ?>
					<li data-target="#slides" id="ind-<?php echo $i; ?>" data-slide-to="<?php echo $i; ?>"></li>
					<?php endfor; ?>
				</ol>
				<div class="carousel-inner">
					<?php for ($i=0; $i < count($content); $i++) : ?>
					<div id="itm-<?php echo $i; ?>" class="carousel-item">
						<img src="./assets/projects/<?php echo $dir; ?>/<?php echo $content[$i]["img"]; ?>.jpg">
					</div>
					<?php endfor; ?>
				</div>
				<a class="carousel-control-prev" href="#slides" role="button" data-slide="prev">
					<i class="fa fa-angle-left"></i>
				</a>
				<a class="carousel-control-next" href="#slides" role="button" data-slide="next">
					<i class="fa fa-angle-right"></i>
				</a>
			</div>

		</div>
		</div>
		<br>

		<!-- Modal Caption (Image Text) -->
		<div id="caption"></div>
	</div>

	<!-- Footer section -->
	<footer></footer>

	<!-- Button Up -->
	<a href="#head" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

	<!-- JavaScript Libraries -->
	<script src="lib/jquery/jquery.min.js"></script>
	<script src="lib/jquery/jquery-1.10.2.min.js"></script>
	<script src="lib/bootstrap/js/bootstrap.min.js"></script>

	<script>
		$('.carousel').carousel({
			interval: false
		}); 
		//Page fade in when load
		$(window).load(function() {
			$("body").animate({opacity: 1}, 1000);
		});

		$(document).ready(function() {
			//Page fade out when unload
			$( "#fade" ).click(function(e) {
				e.preventDefault();
				$link = $(this).attr("href");
				$("body").animate({opacity: 0}, 1000, function(){
					window.location = $link;
				});
			});

			// Add smooth scrolling to all links
			$("a").on('click', function(event) {

				// Make sure this.hash has a value before overriding default behavior
				if (this.hash !== "") {
					// Prevent default anchor click behavior
					event.preventDefault();

					// Store hash
					var hash = this.hash;

					// Using jQuery's animate() method to add smooth page scroll
					// The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
					$('html, body').animate({
						scrollTop: $(hash).offset().top
					}, 800, function(){

						// Add hash (#) to URL when done scrolling (default click behavior)
						window.location.hash = hash;
					});
				} // End if
			});

			// modal close function
			var modal = $("#myModal").children(".modal-content").clone();
			$(".close").on('click', function(event) {
				$("#myModal").children(".modal-content").remove();
				modal.clone().appendTo($("#myModal"));
				$('#myModal').css('display','none');
			});
		});
	</script>

</body>
</html>